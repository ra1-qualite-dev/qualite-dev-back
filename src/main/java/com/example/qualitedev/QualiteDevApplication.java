package com.example.qualitedev;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QualiteDevApplication {
	public static void main(String[] args) {
		SpringApplication.run(QualiteDevApplication.class, args);
	}

}
